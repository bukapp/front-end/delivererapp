
//import router from '@/router';
import axios from 'axios';
//import jwt_decode from "jwt-decode";
//import interceptorsSetup from '../../../services/interceptors'

const state = {
  DeliveriesList: {},
  HandleDelivery:{},
  FinishDelivery:{},
}

const mutations = {
  GET_DELIVERIES_LIST(state: any, payload: any) {
    state.DeliveriesList = payload;
  },
  GET_HANDLE_DELIVERY(state: any, payload: any) {
    state.HandleDelivery = payload;
  },
  PUT_FINISH_DELIVERY(state: any, payload: any) {
    state.FinishDelivery = payload;
  },
}

const actions = {
  getDeliveriesList({ commit }: any) {
    axios.get(`/gateway/deliveries/`).then((response) => {
      commit('GET_DELIVERIES_LIST', response.data)
    });
  },
  getHandleDelivery({ commit }: any, id: any) {
    axios.get(`/gateway/deliveries/handle/${id}`).then((response) => {
      commit('GET_HANDLE_DELIVERY', response.data)
    });
  },
  putFinishDelivery({ commit }: any, body: any) {
    axios.put(`/gateway/deliveries/handle/${body.id}`, body.status).then((response) => {
      commit('PUT_FINISH_DELIVERY', response.data)
    });
  },
}

const getters = {
    deliveries: (state: any) => state.DeliveriesList,
    delivery: (state: any) => state.HandleDelivery,
    putDelivery: (state: any) => state.FinishDelivery,
    }

const deliveryModule = {
  state,
  mutations,
  actions,
  getters
}

export default deliveryModule;