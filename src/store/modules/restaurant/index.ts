import axios from 'axios';

const state = {
  Restaurant: {},
}

const mutations = {

  GET_RESTAURANT(state: any, payload: any) {
    state.Restaurant = payload;
  },
}

const actions = {
  getRestaurant({ commit }: any, id: any) {
    axios.get(`/gateway/restaurants/user/${id}`).then((response) => {
      commit('GET_RESTAURANT', response.data)
    });
  }
}

const getters = {
  restaurant: (state: any) => state.SelectedRestaurant,
}

const restaurantModule = {
  state,
  mutations,
  actions,
  getters
}

export default restaurantModule;
