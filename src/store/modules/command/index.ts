import axios from 'axios';

const state = {
  Command: {},
}

const mutations = {
  GET_COMMAND(state: any, payload: any) {
    state.Command = payload;
  },
}

const actions = {
    getCommand({ commit }: any, id: any) {
        axios.get(`/gateway/commands/${id}`).then((response) => {
          commit('GET_COMMAND', response.data)
          console.log(response.data)
        });
  },
}

const getters = {
    command: (state: any) => state.command,
    }
    

const commandModule = {
  state,
  mutations,
  actions,
  getters
}

export default commandModule;