import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import delivery from './modules/delivery'
import command from './modules/command'
import restaurant from './modules/restaurant'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    delivery,
    user,
    command,
    restaurant,
  }
})
